#! /bin/bash

VERSION=1.1
DISTDIR=desktop-file-validator-$VERSION

make clean
mkdir -p $DISTDIR
cp *.spec *.h *.c *.py *.sh Makefile mkinstalldirs $DISTDIR
tar zcvf $DISTDIR.tar.gz $DISTDIR/*
