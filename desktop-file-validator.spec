Summary: Validates .desktop files
Name: desktop-file-validator
Version: 1.0
Release: 1
URL: http://www.freedesktop.org/standards/
Source0: %{name}-%{version}.tar.gz
License: LGPL
Group: Development/Tools
BuildRoot: %{_tmppath}/%{name}-root

%description
.desktop files are used to describe an application for inclusion in GNOME or KDE menus.
This small program checks whether a .desktop file complies with the specification 
at http://www.freedesktop.org/standards/

%prep
%setup -q

%build
make

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT prefix=/usr sysconfdir=/etc

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root)
%{_bindir}/desktop-file-validator

%changelog
* Mon Oct  8 2001 Havoc Pennington <hp@redhat.com>
- Initial build.


