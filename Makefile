CFLAGS=-g -Wall `pkg-config glib-2.0 --cflags`
LDFLAGS=`pkg-config glib-2.0 --libs`
prefix=/usr/local
bindir=$(prefix)/bin

validate: gnome_desktop_file.o validate.o
	gcc $(CFLAGS) $(LDFLAGS) gnome_desktop_file.o validate.o -o validate

install: validate
	./mkinstalldirs $(DESTDIR)$(bindir) \
	&& install -m 755 validate $(DESTDIR)$(bindir)/desktop-file-validator

clean:
	/bin/rm -f *.o validate
